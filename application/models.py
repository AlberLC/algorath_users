from application import db

follow = db.Table(
    'follow',
    db.Column('id_follower', db.Integer, db.ForeignKey('user.id'), primary_key=True),
    db.Column('id_following', db.Integer, db.ForeignKey('user.id'), primary_key=True)
)


class User(db.Model):
    __tablename__ = 'user'
    id_ = db.Column('id', db.Integer, primary_key=True, autoincrement=True)
    username = db.Column('username', db.String(50), unique=True, nullable=False)
    password = db.Column('password', db.String(50), nullable=False)
    email = db.Column('email', db.String(100), unique=True, nullable=True)
    realname = db.Column('realname', db.String(50), unique=False, nullable=True)
    followers = db.relationship(
        'User',
        secondary=follow,
        primaryjoin=(follow.c.id_follower == id_),
        secondaryjoin=(follow.c.id_following == id_),
        backref='following')

    def __init__(self, username, password, id_=None, email=None, realname=None):
        self.username = username
        self.password = password
        self.id_ = id_
        self.realname = realname
        self.email = email
