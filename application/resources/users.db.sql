BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "user" (
	"id"	INTEGER PRIMARY KEY AUTOINCREMENT,
	"username"	text NOT NULL UNIQUE,
	"password"	TEXT NOT NULL,
	"email"	text UNIQUE,
	"realname"	text
);
CREATE TABLE IF NOT EXISTS "follow" (
	"id_follower"	int,
	"id_following"	int,
	FOREIGN KEY("id_follower") REFERENCES "user"("id"),
	FOREIGN KEY("id_following") REFERENCES "user"("id"),
	PRIMARY KEY("id_follower","id_following")
);
INSERT INTO "user" VALUES (1,'Stacy','stacy',NULL,NULL);
INSERT INTO "user" VALUES (2,'Flanagan','flanagan','flanagan@email.com','Alejandro');
INSERT INTO "user" VALUES (3,'Pepe','pepe',NULL,NULL);
INSERT INTO "user" VALUES (4,'María','maria',NULL,NULL);
INSERT INTO "user" VALUES (5,'Javier','javier',NULL,NULL);
INSERT INTO "follow" VALUES (1,2);
INSERT INTO "follow" VALUES (3,2);
INSERT INTO "follow" VALUES (2,1);
COMMIT;
