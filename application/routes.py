import json
from flask import render_template, request, redirect, url_for, session
from application.models import User
from application import app, db


@app.route('/', methods=['GET', 'POST'])
def login():
    if request.method == 'GET':
        return render_template('login.html')
    else:
        name = request.form.get('name')
        pass1 = request.form.get('password')
        user = User.query.filter_by(username=name).first()
        if user and user.password == pass1:
            session['messages'] = json.dumps({'name': name})
            return redirect(url_for('user'))
        else:
            return redirect(url_for('login'))


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'GET':
        return render_template('signup.html')
    else:
        name = request.form.get('name')
        pass1 = request.form.get('pass1')
        pass2 = request.form.get('pass2')
        if pass1 == pass2:
            db.session.add(User(name, pass1))
            db.session.commit()
            return redirect(url_for('login'))
        else:
            return redirect(url_for('signup'))


@app.route('/user', methods=['GET', 'POST'])
def user():
    if request.method == 'GET':
        messages = session['messages']
        name = json.loads(messages)['name']
        user = User.query.filter_by(username=name).first()
        return render_template(
            'user.html',
            user=user,
            followers=user.followers,
            following=user.following,
            all_users=User.query.filter(User.username != name)
        )
    else:
        username = request.form.get('username')
        user = User.query.filter_by(username=username).first()
        follow_username = request.form.get('follow_username')
        follow_user = User.query.filter_by(username=follow_username).first()
        user.following.append(follow_user)
        db.session.commit()

        return redirect(url_for('user'))


@app.route('/edituser', methods=['GET', 'POST'])
def edituser():
    if request.method == 'GET':
        name = request.args.get('username')
        user = User.query.filter_by(username=name).first()
        return render_template('edituser.html', user=user)
    else:
        name = request.form.get('username')
        user = User.query.filter_by(username=name).first()
        email = request.form.get('email')
        realname = request.form.get('realname')

        user.email = email
        user.realname = realname
        db.session.commit()
        return redirect(url_for('user'))


@app.route('/deleteuser')
def deleteuser():
    name = request.args.get('username')
    user = User.query.filter_by(username=name).first()
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('login'))


@app.route('/unfollow', methods=['POST'])
def unfollow():
    username = request.form.get('username')
    user = User.query.filter_by(username=username).first()
    follow_username = request.form.get('follow_username')
    follow_user = User.query.filter_by(username=follow_username).first()
    user.following.remove(follow_user)
    db.session.commit()

    return redirect(url_for('user'))
